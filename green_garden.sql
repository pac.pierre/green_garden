-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 15 mars 2022 à 18:03
-- Version du serveur : 10.4.22-MariaDB
-- Version de PHP : 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `green_garden`
--

DELIMITER $$
--
-- Procédures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `moyenneDelai` ()  BEGIN
SELECT commandeId, AVG(datediff(datePaiement, dateCommande)) as delaiMoyen
FROM `factures`;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `archive`
--

CREATE TABLE `archive` (
  `clientId` int(11) NOT NULL,
  `commandeId` int(11) NOT NULL,
  `dateCommande` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `avoir`
--

CREATE TABLE `avoir` (
  `produitId` int(11) NOT NULL,
  `categoriesId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `categoriesId` int(11) NOT NULL,
  `nom_categorie` varchar(50) NOT NULL,
  `categoriesId_1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`categoriesId`, `nom_categorie`, `categoriesId_1`) VALUES
(1, 'été', 1),
(2, 'autonme', 2),
(3, 'printemps', 3),
(4, 'hiver', 4),
(5, 'exterieur', 5),
(6, 'intérieur', 6);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `clientId` int(11) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `zipcode` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pwd` varchar(50) NOT NULL,
  `newsletter` tinyint(1) NOT NULL,
  `logIn` varchar(50) NOT NULL,
  `commercialId` int(11) NOT NULL,
  `nomination` enum('particulier','professionnel') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`clientId`, `gender`, `name`, `surname`, `street`, `zipcode`, `city`, `phone`, `email`, `pwd`, `newsletter`, `logIn`, `commercialId`, `nomination`) VALUES
(1, 'F', 'Shepard', 'Jane', 'oui', '0', 'lost', '000000', 'email', '903b9c544a82e6eac5052f216265bf045dce8aea', 1, 'monlogin', 1, 'particulier'),
(2, 'F', 'Hiroko', 'Ayala', 'Ap #506-9798 Rutrum Av.', '15655', 'Roubaix', '07 89 42 64 50', 'egestas.lacinia.sed@protonmail.ca', '7ce572ffdbb2ba22af68fc66aa298aa7fc683339', 0, 'Lamb, Dexter L.', 4, 'particulier'),
(3, 'F', 'Yuli', 'Hamilton', '858-7131 Proin Street', '79248', 'Hénin-Beaumont', '03 15 68 76 18', 'nulla.tincidunt@icloud.net', '1a1ffb0e4bfc29a6139ca36991a07b034fa4410e', 1, 'Morrow, Dane T.', 3, 'professionnel'),
(4, 'M', 'Todd', 'Perry', '113-9195 Lorem Av.', '17908', 'Valenciennes', '07 99 22 54 57', 'aliquam.ultrices.iaculis@yahoo.com', '4e3a038612afbc128cbbd7a932529c5271f94782', 0, 'Craig, Rina Z.', 1, 'particulier'),
(5, 'F', 'Florence', 'Jennings', 'Ap #207-4808 Nulla Avenue', '36324', 'Quimper', '02 51 73 25 71', 'nulla@hotmail.couk', 'fe135dba3103e67f0c861978f3a726a7cd25cc38', 0, 'Hatfield, Alma N.', 3, 'professionnel'),
(6, 'F', 'Jane', 'Guerrero', 'P.O. Box 930, 3515 Netus Rd.', '11723', 'Pau', '07 25 43 29 78', 'suscipit.est.ac@outlook.net', '65a58984c9e32e86a9032e6fb0c812c6901597c0', 0, 'Fitzgerald, Thomas J.', 3, 'professionnel'),
(7, 'M', 'Yuri', 'Fry', '6085 Ut St.', '47388', 'Épernay', '06 57 46 84 56', 'eleifend.cras@aol.couk', 'b6a0370321f3cbb8d37b4d0b1b64bf4f315705aa', 1, 'Rice, Xandra G.', 5, 'professionnel'),
(8, 'M', 'Griffith', 'Glover', 'Ap #131-6283 Mauris Ave', '47595', 'Haguenau', '05 76 74 02 81', 'sit.amet@protonmail.edu', '43df3bbb8f2cd6e736a1ebf89f6ba6d9082642ba', 1, 'Buckley, Xena E.', 4, 'particulier'),
(9, 'M', 'Frances', 'Carpenter', '9401 Semper. Street', '07849', 'Quimper', '03 37 76 23 86', 'turpis.nulla@outlook.net', '7ac84da5abb17b8a9e3f1c38e3f9bba073343337', 0, 'Barrera, Ezra Q.', 3, 'particulier'),
(10, 'M', 'Sage', 'Cross', 'P.O. Box 618, 5289 Maecenas Rd.', '70778', 'Perpignan', '07 29 75 69 35', 'ullamcorper.velit.in@yahoo.com', '862324de7d8f0e0e54bd668f4b2c1efa66c1989f', 1, 'Andrews, Jesse O.', 2, 'professionnel'),
(11, 'M', 'Rashad', 'Wolfe', '4528 Vulputate Street', '19533', 'Bordeaux', '04 57 40 31 01', 'natoque.penatibus@icloud.org', '6a2e0c92791d6e8d66864289ad508dfaab7fe96b', 1, 'Wilkinson, Gisela D.', 5, 'particulier'),
(12, 'M', 'Logan', 'Harrison', '502 Dolor Rd.', '55421', 'Dieppe', '08 68 02 77 46', 'quis@icloud.com', '0128b62890ca4cb7124bfe5b2b8ed9a3df744bef', 0, 'Hewitt, Linus A.', 2, 'professionnel'),
(13, 'M', 'Jonah', 'Sawyer', 'P.O. Box 128, 1452 Commodo Avenue', '23690', 'Auxerre', '08 85 64 27 73', 'in.lorem@google.net', 'd25fd6edbdbfb540aafe050c23f769aa754a3e1b', 1, 'Hunt, Slade J.', 3, 'particulier'),
(14, 'F', 'Oleg', 'Kerr', 'Ap #663-9905 Euismod St.', '66456', 'Saint-Lô', '07 36 28 61 63', 'magna.et@yahoo.com', '8a11effeaae0533dcb5474db4bceda3a9e4e996a', 1, 'Delacruz, Teagan N.', 2, 'professionnel'),
(15, 'F', 'Brielle', 'Clements', '3168 Lobortis Ave', '90887', 'Laon', '04 28 23 84 40', 'odio.vel@icloud.ca', '80c4fb95c1d942c41a7e039e7d5abc72bb377e20', 1, 'Joyner, Jonah Q.', 2, 'particulier'),
(16, 'F', 'Dana', 'Stark', 'P.O. Box 325, 1167 Diam Av.', '92105', 'Abbeville', '02 72 18 63 14', 'eget@outlook.edu', 'f461c2d9d2d9a286150cb1e9989e706d5be87927', 1, 'Ayala, Blake K.', 3, 'particulier'),
(17, 'M', 'Peter', 'Levy', '722-481 Quis Street', '45183', 'Antibes', '05 14 74 98 33', 'justo.faucibus@yahoo.net', '02d94e457d7883f3e4678e8f702fa54304ecb46f', 1, 'Patterson, Tara F.', 5, 'particulier'),
(18, 'F', 'Martena', 'Sweet', 'Ap #395-1560 Libero. Road', '45534', 'Épernay', '07 52 34 22 46', 'cras.interdum@protonmail.couk', '64a65c06d6011c158e8c6765eee79032f58faaac', 0, 'Fernandez, Rana T.', 1, 'professionnel'),
(19, 'F', 'Selma', 'Wilcox', '1854 Mauris Road', '76013', 'Créteil', '07 16 41 21 28', 'urna.nunc.quis@hotmail.com', '1ad9e4cfaf71ec3ed8451ea314968a5b4163b5b0', 1, 'Craft, Henry Q.', 2, 'professionnel'),
(20, 'F', 'Myra', 'Wolf', 'Ap #810-959 Sed Rd.', '75408', 'Montbéliard', '03 66 62 53 36', 'ullamcorper.velit@protonmail.org', '4c530b800154f9353bc01ecd6ec2d2a7270e6302', 0, 'Randolph, Joy K.', 2, 'particulier'),
(21, 'F', 'Morgan', 'Mcdaniel', 'Ap #446-4833 Scelerisque Avenue', '64115', 'Aurillac', '05 55 98 88 82', 'morbi.quis@outlook.com', '27121713ad2ae2d7de8c189c7d232874c8393b0c', 0, 'Porter, Kieran P.', 4, 'professionnel'),
(22, 'M', 'Nigel', 'Yang', 'Ap #954-1970 Sed Road', '36613', 'Aulnay-sous-Bois', '08 21 74 65 55', 'in@icloud.org', '3d61fcb059d0e6d978bf57cd9967fb2b9c9e5336', 1, 'Hoover, Nyssa H.', 1, 'particulier'),
(23, 'M', 'Davis', 'Barnes', 'Ap #299-7189 Venenatis St.', '52833', 'Thionville', '07 76 59 24 46', 'senectus@aol.edu', 'a4c5dd543cb79f95b5554d201865b320d265b81d', 0, 'Robertson, Leah J.', 2, 'professionnel'),
(24, 'F', 'Vivian', 'Buchanan', 'Ap #845-8427 Morbi Street', '98335', 'Épernay', '02 55 00 14 61', 'neque.morbi@aol.couk', 'f759a3c900963e6a47d5cd79d49aff694d4c80b3', 1, 'Crawford, Shoshana X.', 5, 'particulier'),
(25, 'F', 'Galena', 'House', '663-8434 Id, Avenue', '34834', 'Sens', '07 75 65 93 71', 'primis.in@outlook.com', 'e19bc59b8c59a3a5e97fb4d57c16a96d18cb3e71', 1, 'Gamble, Amos M.', 4, 'professionnel'),
(26, 'M', 'Ishmael', 'Garrett', '955-2031 Ipsum Ave', '83312', 'Bordeaux', '03 16 41 53 08', 'magna@google.couk', 'd2544e8e3b81044c5821a10443f4b9ef43ffd5f4', 1, 'Hall, Sophia K.', 3, 'professionnel'),
(27, 'M', 'Jasper', 'Hines', '3508 Aenean St.', '32097', 'Clermont-Ferrand', '07 22 53 64 13', 'dui@aol.com', '9f3048c25c0d70121c09f004b7c070ca108cb933', 0, 'Dotson, Callie S.', 2, 'particulier'),
(28, 'M', 'Logan', 'Ramos', 'Ap #243-5743 Eget Ave', '73345', 'Avignon', '04 54 13 79 82', 'tempus.scelerisque@protonmail.ca', '91c9734cec03c6f5747990961ad4fc0198061b28', 0, 'Knowles, Marsden H.', 2, 'professionnel'),
(29, 'F', 'Hedley', 'Duke', 'Ap #737-9722 Ante St.', '59288', 'Saint-Lô', '06 94 87 19 15', 'vivamus.euismod@google.couk', 'cda83595aa66b7b72428a3f88089fcec5e823b56', 0, 'Roman, Edan C.', 5, 'particulier'),
(30, 'F', 'Scarlet', 'Pope', '521-5808 Vestibulum Rd.', '87764', 'Toulouse', '07 42 34 73 35', 'phasellus.vitae@aol.ca', '0343c4f520b01122e75b59096f5c173363b0f666', 0, 'Alexander, Elvis F.', 2, 'particulier'),
(31, 'F', 'Lila', 'Lynch', '4785 Morbi St.', '75118', 'Brive-la-Gaillarde', '08 20 51 88 13', 'tellus@yahoo.org', 'b57d2336d8237cab4d2acc1a5374fec154a1bde6', 1, 'Parker, Deborah S.', 3, 'professionnel'),
(32, 'M', 'Clarke', 'Castillo', 'P.O. Box 444, 8592 Sed, Street', '52562', 'Castres', '04 84 41 63 63', 'scelerisque.neque@aol.com', '96bfc58fded510f6be630c1624d6604c2def76f5', 0, 'Carson, Robin J.', 1, 'professionnel'),
(33, 'M', 'Raven', 'Workman', '581 Ullamcorper Avenue', '85244', 'Bastia', '04 20 16 72 93', 'massa@google.couk', '221d564fd03c26aa5dc7b9dceb0e2ce429eb43c0', 1, 'Estes, Bertha R.', 5, 'professionnel'),
(34, 'F', 'Ignacia', 'Slater', '6705 Natoque St.', '71507', 'Montluçon', '03 16 16 33 83', 'tristique.ac.eleifend@hotmail.com', '44f5fb8154361a9a22fe135eba5fd624bac01748', 0, 'Barton, Quon L.', 5, 'particulier'),
(35, 'M', 'Ashton', 'Le', 'Ap #577-1726 Libero St.', '12164', 'Dreux', '04 72 61 34 96', 'sociis.natoque@google.couk', '1e1885c2ba6e5f56fe5685153ea808fd630c61de', 1, 'Mcconnell, Guinevere U.', 2, 'particulier'),
(36, 'M', 'Mason', 'Baker', '4958 Tempus St.', '52021', 'Brive-la-Gaillarde', '03 18 85 73 61', 'mauris.ipsum@yahoo.net', 'e0f1da6a7db1b9918862bd0212d1be3f4b9b950c', 0, 'Mcleod, Travis R.', 2, 'professionnel'),
(37, 'F', 'Nina', 'Mccall', '910-1581 Et, St.', '48105', 'Saint-Nazaire', '04 30 75 33 87', 'odio.vel@yahoo.ca', '06ae9ed5892f58354933e63b3869cd6af34d54c1', 0, 'Mccray, Jonah T.', 4, 'particulier'),
(38, 'F', 'Jakeem', 'Larsen', 'Ap #623-5666 Porttitor Avenue', '40411', 'Sens', '07 76 83 11 88', 'non.sollicitudin@hotmail.couk', 'd97f66e51f633648daa62c3365b263a84b87e6d6', 0, 'Leonard, Hamish E.', 2, 'professionnel'),
(39, 'M', 'Levi', 'Lewis', '312-8057 Cursus. Avenue', '00977', 'Nanterre', '08 31 12 84 65', 'mauris.vestibulum@protonmail.couk', '7530188321e0b70b070c07f4b248072dcf79c1fd', 0, 'Norris, MacKenzie D.', 4, 'professionnel'),
(40, 'M', 'Ferris', 'Roberson', '356-5633 Non, Street', '67054', 'Tarbes', '02 22 37 15 45', 'habitant.morbi@icloud.couk', '4fbd7423a59a7b30b7be8a131dd623088f38dea4', 1, 'Shepard, Benedict S.', 4, 'professionnel'),
(41, 'M', 'Silas', 'Odom', '1396 Tincidunt Av.', '88277', 'Quimper', '09 86 89 63 98', 'et.rutrum@aol.edu', '7be6262530b78e879cb569ad73be614f827e59b2', 1, 'Simpson, Lars U.', 3, 'particulier'),
(42, 'F', 'Chaney', 'Russo', 'Ap #528-9002 Sapien St.', '72582', 'Rodez', '05 67 76 96 25', 'sapien.aenean@google.ca', '2ab41df0c57b80a71fcc4338a9dd6680a8db3a89', 1, 'Mills, Emily G.', 4, 'particulier'),
(43, 'M', 'Erin', 'Bartlett', 'Ap #427-3154 Tristique Rd.', '97871', 'Lanester', '04 11 11 09 41', 'aenean.gravida@yahoo.couk', '10707e32ea61667a52fb98e39bad0d7abd4c3292', 0, 'Becker, Caldwell R.', 3, 'professionnel'),
(44, 'F', 'Kim', 'Dodson', 'P.O. Box 644, 9086 Neque Road', '14534', 'Limoges', '04 40 24 02 38', 'interdum@google.net', '32e553baa9629807019dd491e1b56d79a14716df', 0, 'Lewis, Eugenia W.', 2, 'particulier'),
(45, 'M', 'Karen', 'Mayo', 'P.O. Box 630, 3273 Nisl. Rd.', '79656', 'Mérignac', '03 82 15 33 28', 'nibh@protonmail.edu', '72afabeddcde10cf5f6e94b915f1df4ca1c78706', 1, 'Simpson, Xavier O.', 4, 'particulier'),
(46, 'M', 'Callum', 'Page', '736-4181 Quis Ave', '34496', 'Béziers', '01 33 53 46 76', 'sodales.nisi@yahoo.net', '60a2d48d19aaf23306d5f5eaeb08d85205a53e6e', 0, 'Alvarado, Julian Y.', 2, 'particulier'),
(47, 'M', 'Lawrence', 'Atkinson', 'P.O. Box 377, 1026 Diam. Av.', '77520', 'Brest', '08 34 60 78 56', 'ad.litora@hotmail.couk', '0da6b5158fe70b71e7b9249d33509e546fd51de1', 0, 'Martinez, Colt B.', 4, 'professionnel'),
(48, 'M', 'Nigel', 'Jensen', 'Ap #625-6526 Ut Ave', '07591', 'Toulon', '03 53 07 05 54', 'pede.cum@protonmail.net', '621dee4479c6dc82941f70f759dec1642a5f13fe', 1, 'Andrews, Steven S.', 2, 'professionnel'),
(49, 'F', 'Leigh', 'Knox', '926-9230 Aliquam St.', '10877', 'Belfort', '08 24 90 76 14', 'faucibus@hotmail.couk', 'c541f83366f521c7be85f5830e47870db4ab445b', 0, 'Ortiz, Freya Q.', 5, 'professionnel'),
(50, 'M', 'Kadeem', 'Mccormick', 'Ap #543-6931 Eget Rd.', '23556', 'Maubeuge', '06 92 94 57 74', 'eu.sem.pellentesque@yahoo.ca', '51550d50ab27d0f3cb097e033002b9c1d33afbd5', 1, 'Combs, Josiah S.', 3, 'professionnel'),
(51, 'F', 'Nehru', 'Fry', 'Ap #720-183 Semper Ave', '44750', 'Saintes', '03 02 18 33 32', 'praesent.interdum@icloud.edu', '776974983f08812e29f20f2fd7cfbbebd10698a3', 0, 'Key, Brynne W.', 2, 'particulier'),
(52, 'F', 'Azalia', 'Stein', 'P.O. Box 638, 2483 Eget, Av.', '26781', 'Aurillac', '06 41 20 15 77', 'aliquam.adipiscing@aol.com', '60ac2f8b376b867c0cca64250a9878b45ea2009e', 1, 'Castro, Ariel F.', 3, 'professionnel'),
(53, 'F', 'Erica', 'Herrera', 'Ap #463-3381 In, Avenue', '12243', 'Laon', '02 21 16 73 93', 'lorem.semper@outlook.ca', 'cd7402116d9625e05f71ec6b9a13ae6c234716b6', 0, 'Hester, Jakeem X.', 4, 'particulier'),
(54, 'F', 'Maya', 'Barlow', '722-1985 Imperdiet Road', '58362', 'Troyes', '05 26 37 06 55', 'amet.luctus@google.net', 'b938f2f0adb48c4a4bde60966d3b428a31504833', 0, 'Marshall, Idona N.', 4, 'particulier'),
(55, 'M', 'Wang', 'Keith', '245 Natoque Rd.', '33677', 'Toulouse', '07 73 34 10 11', 'magna@aol.couk', '7bf4a435a22da54480c29035865596d4996d21ec', 1, 'Grimes, Adria X.', 4, 'professionnel'),
(56, 'F', 'Doris', 'Warren', 'Ap #194-2889 Luctus Rd.', '88553', 'Dunkerque', '03 62 63 82 41', 'lacus.quisque@google.org', '270bf8fd549e39a5eda99fc9282faa3660891401', 0, 'Craft, Hope F.', 4, 'particulier'),
(57, 'M', 'August', 'Henry', '2331 Accumsan Av.', '51026', 'Dreux', '05 10 64 51 11', 'dui@yahoo.org', '2150c61090d801e93d2e28464b14856cceb2a931', 0, 'Mccray, Macaulay X.', 4, 'professionnel'),
(58, 'F', 'Zorita', 'Hyde', 'P.O. Box 850, 8011 Sed Street', '70832', 'Quimper', '09 84 86 33 28', 'lectus.rutrum@aol.edu', '2c66aa8731175cae900836d58f74727ed4b79665', 1, 'Weaver, Thor U.', 2, 'particulier'),
(59, 'F', 'Cecilia', 'Horn', '943-6025 Elit. St.', '58782', 'Saint-Lô', '03 66 26 21 34', 'blandit.congue.in@yahoo.com', 'f0999930e866af96a8d6fe6371c0a98c6aad2549', 1, 'Aguilar, Willa B.', 1, 'professionnel'),
(60, 'M', 'Zahir', 'Avery', '638-8629 Nisl St.', '46475', 'Marcq-en-Baroeul', '08 15 22 47 11', 'magna@yahoo.com', 'd0d8a29f4a7ab09bde58ca1edda0d311ee7f3cdd', 1, 'Carpenter, Leo A.', 1, 'professionnel'),
(61, 'F', 'Hayfa', 'Wood', 'P.O. Box 789, 584 Morbi St.', '27814', 'Brive-la-Gaillarde', '02 33 81 10 84', 'praesent.eu@hotmail.org', 'e08f4a67dca758cb51be46489a727f366dc9ffa5', 1, 'Kelly, Brock N.', 3, 'professionnel'),
(62, 'M', 'Giacomo', 'Rush', 'Ap #385-3955 Suspendisse Ave', '63556', 'Le Petit-Quevilly', '06 55 85 12 17', 'ut.odio.vel@outlook.org', '8fa9fb0a2e0c013c127a757969a8b1154906f429', 0, 'Hunt, Moana E.', 4, 'particulier'),
(63, 'M', 'Wynter', 'Burton', 'P.O. Box 416, 9417 Felis. Street', '14384', 'Dole', '03 11 67 08 63', 'feugiat.sed@protonmail.couk', 'b48b0d4a36acf54d2388594084c02fc9d85da976', 1, 'Ballard, India Q.', 2, 'particulier'),
(64, 'M', 'Simon', 'Sharpe', '434-9605 Erat Ave', '38756', 'Le Puy-en-Velay', '07 09 61 15 67', 'mollis@icloud.com', '75f09f083bf7575dc86f105ccc3e6b9c19e87252', 0, 'Molina, Drake G.', 5, 'professionnel'),
(65, 'M', 'Randall', 'Shepherd', '609-7007 Pede. Rd.', '18058', 'Saint-Malo', '08 45 51 15 77', 'quis.turpis@google.couk', 'b0bb9bdc0d6bfc121c8e4d083aa16273e8c37a8f', 1, 'Mcmillan, Jerry T.', 2, 'professionnel'),
(66, 'F', 'Ivana', 'Osborn', 'P.O. Box 582, 3040 Ac St.', '87504', 'Limoges', '07 74 25 85 65', 'integer.eu@google.ca', '60358c923bead5b65e84c15b3e5cae83d0e33904', 0, 'Mathews, Dieter X.', 2, 'professionnel'),
(67, 'M', 'Emerson', 'Odom', '666-2935 Ac, St.', '24410', 'Nevers', '05 73 35 83 12', 'facilisis.vitae.orci@aol.couk', '343e4b812a414c6e8d5d7d43d7b7aee70b35b8c8', 0, 'Doyle, Nero C.', 1, 'particulier'),
(68, 'M', 'Lyle', 'Hull', 'P.O. Box 764, 7771 Dolor, Avenue', '92375', 'Sens', '05 95 21 73 27', 'vitae@hotmail.edu', 'c01ff4de53d6b082988b674689891845115c5201', 0, 'Terry, Preston N.', 4, 'particulier'),
(69, 'M', 'Stewart', 'Mckenzie', '8461 Quis, Avenue', '86337', 'Limoges', '06 76 87 14 59', 'tempus.eu@hotmail.org', '52492bcac0dd1e384fdf373f4051608bc96eb88d', 0, 'Munoz, Knox L.', 5, 'particulier'),
(70, 'M', 'Lev', 'Cline', 'P.O. Box 644, 6557 Accumsan Avenue', '24485', 'Ajaccio', '03 18 96 56 26', 'purus.duis@protonmail.edu', '703308225f37b80f34bc7690442d1d4d498e9010', 0, 'Forbes, Margaret M.', 2, 'professionnel'),
(71, 'F', 'Jamalia', 'Spencer', 'P.O. Box 931, 5807 Dolor. Road', '28507', 'Lille', '03 15 33 46 21', 'fringilla.cursus@google.ca', 'd1e727f6966bdcb57e9230bbc8fe9eb536cf8d5e', 0, 'Ballard, Daquan I.', 1, 'particulier'),
(72, 'M', 'Walker', 'Boone', '1341 At Street', '01221', 'Albi', '02 68 63 15 41', 'proin@google.com', '3fa824935387b89eeb706487d27a655f3faf3b9c', 1, 'Robbins, Zeus E.', 4, 'particulier'),
(73, 'M', 'Merritt', 'Whitehead', '573-2685 Iaculis Rd.', '84520', 'Charleville-Mézières', '04 03 00 34 62', 'sed.eget@aol.couk', 'aeb9d26ba227661355e1297dd8e6b60ae4eab6bf', 1, 'Wright, Shannon V.', 1, 'particulier'),
(74, 'M', 'Hop', 'Haley', 'Ap #770-4518 Diam. Road', '03862', 'Saint-Louis', '02 88 27 22 28', 'phasellus.nulla@google.net', '2c78608a4ed7e0027f729cb9746aac4d19879266', 0, 'Shaffer, Ali M.', 5, 'particulier'),
(75, 'M', 'Noble', 'Tran', 'Ap #775-8162 Lobortis Road', '75521', 'Dijon', '07 83 01 92 53', 'nulla.cras.eu@aol.edu', '914183f1cada2f0b5d011f9214115c78576eb9c5', 1, 'Oliver, Allen O.', 1, 'professionnel'),
(76, 'F', 'Linda', 'Bowers', '751-2013 Arcu. St.', '16496', 'Rouen', '05 15 77 22 86', 'gravida@hotmail.couk', 'adaebb6b170a1ab3991a51f94d1c4a1fe1b21fe5', 1, 'Coffey, Ignatius I.', 3, 'particulier'),
(77, 'M', 'Curran', 'French', '5707 Sit Rd.', '32540', 'Mont-de-Marsan', '03 52 85 21 21', 'lorem.semper@yahoo.com', 'ef6ff48be9df23acf63811e0965bee98327515ab', 1, 'King, Dean S.', 3, 'particulier'),
(78, 'F', 'Kalia', 'Serrano', '729-7454 Duis Street', '85804', 'Paris', '06 18 37 09 54', 'mi@outlook.edu', '09d778413e0acb28d076338003b0037388825d1c', 0, 'Ayers, Christen Q.', 3, 'professionnel'),
(79, 'F', 'Tara', 'Woodward', '616-4752 Donec St.', '23722', 'Béthune', '02 71 42 13 31', 'dolor.dapibus.gravida@yahoo.com', '92e6c5d21fdf940ad4671bcf403c92d4ff02ad38', 1, 'Holt, Harlan Q.', 5, 'professionnel'),
(80, 'M', 'Caesar', 'Ballard', '793-4744 Morbi Street', '63858', 'Vichy', '06 41 31 39 16', 'fringilla.cursus@yahoo.edu', 'ff837726d0cb0a0555f04102a37e127da10bb748', 1, 'Campos, Jeanette E.', 4, 'particulier'),
(81, 'M', 'Carlos', 'Rasmussen', '879-1911 Ligula Avenue', '76746', 'Ajaccio', '03 96 23 27 88', 'consectetuer.cursus@hotmail.couk', '63d51e308091cedec5281c35999eae4a02b0728d', 1, 'Henry, Brandon U.', 1, 'professionnel'),
(82, 'F', 'Fleur', 'Massey', 'P.O. Box 658, 1596 Nullam Rd.', '11787', 'Clermont-Ferrand', '08 66 03 32 32', 'eu.tellus@google.edu', '1c34f95709cd80b1dd07267a292ba4b5e2e664d9', 1, 'Glover, Susan F.', 2, 'particulier'),
(83, 'M', 'Jack', 'Gay', 'Ap #412-9954 Natoque Ave', '37562', 'Sarreguemines', '03 52 28 67 43', 'enim.consequat@hotmail.org', '82755016c1363e1128c9ff4ff69ae16484cd24f8', 0, 'Bright, Ezekiel M.', 1, 'professionnel'),
(84, 'M', 'Thomas', 'Sullivan', 'Ap #567-3306 Imperdiet Avenue', '42213', 'Bègles', '03 81 37 08 84', 'a.ultricies@icloud.couk', '8f49fc2de543a12641fda76091dbf157407ff3be', 1, 'White, Hop N.', 2, 'particulier'),
(85, 'M', 'Kasper', 'Bailey', 'P.O. Box 499, 9742 Tincidunt St.', '31867', 'Besançon', '07 53 98 41 62', 'sed.malesuada@protonmail.net', 'a9beaee8d54b56a685b35ee72e7933e515bd86ef', 1, 'Harper, Lenore X.', 1, 'professionnel'),
(86, 'F', 'Lacy', 'Quinn', 'Ap #202-1966 Vivamus Road', '36242', 'Perpignan', '07 63 77 05 08', 'nec.enim.nunc@hotmail.com', 'd62f483ce90309c515a64266ae92e4ce3b4b23a0', 1, 'Hahn, Wayne Z.', 4, 'professionnel'),
(87, 'F', 'Simone', 'Howard', '640-1178 Ligula. Road', '85283', 'Saumur', '08 87 72 34 41', 'ipsum.dolor.sit@protonmail.com', 'fbddd341408e8b0738acf12113c7d9b8ed36926c', 1, 'Reeves, Gloria T.', 4, 'particulier'),
(88, 'M', 'Jerome', 'Armstrong', 'Ap #263-8657 Blandit St.', '68664', 'Istres', '07 13 32 09 51', 'arcu.vivamus.sit@outlook.ca', '20d4611b423b6aa38247b6ed5a4647a1b2d6f33b', 0, 'Brown, Chaney B.', 2, 'particulier'),
(89, 'F', 'Kaseem', 'Castaneda', 'P.O. Box 604, 2325 Lacus, St.', '81223', 'Sens', '08 44 77 66 44', 'libero@aol.ca', '40b4723415978254d9bd2aec94d6a9d3d2ca50d0', 1, 'Johnston, Carson V.', 2, 'particulier'),
(90, 'M', 'Brenden', 'Cardenas', 'P.O. Box 738, 6356 Morbi Ave', '14428', 'Colomiers', '07 61 85 38 85', 'vulputate.risus.a@icloud.net', 'bf1020d1260478483d6161d47d1a50b7165fc2af', 0, 'Nelson, Harriet J.', 4, 'professionnel'),
(91, 'M', 'Rajah', 'Richard', 'P.O. Box 966, 2110 Nam Avenue', '77547', 'Limoges', '06 51 73 36 62', 'eget.ipsum.donec@google.ca', '100d034c37cca66b5994e9a01676963fc46067d5', 1, 'Duke, Lysandra T.', 2, 'professionnel'),
(92, 'M', 'Gregory', 'Mercer', '7770 Mi Road', '44663', 'Montluçon', '05 67 56 41 87', 'pede.ultrices.a@icloud.edu', 'b8683f38289dbb041ae65dbcb24a8998fd893180', 1, 'Silva, Aaron Q.', 1, 'professionnel'),
(93, 'F', 'Karen', 'Cantrell', '451-7107 Arcu. Street', '82564', 'Abbeville', '08 81 51 77 71', 'enim@icloud.ca', '56162b4e315a7a8c45163f158997becd5cf042ca', 0, 'Horn, Iris C.', 5, 'professionnel'),
(94, 'F', 'Ebony', 'Shepard', '9594 Donec Rd.', '63952', 'Belfort', '02 76 57 95 65', 'diam.vel.arcu@outlook.org', '197965b6d7782a0fc54c79f49988c3563a94431d', 1, 'Wolfe, Constance T.', 2, 'particulier'),
(95, 'M', 'Avye', 'Wolf', 'P.O. Box 363, 7125 Sem. Rd.', '81431', 'Dunkerque', '07 74 57 61 50', 'et.risus@aol.ca', '2d7756cdc801662a02b7b682d5f0c25682b1574c', 0, 'Medina, Warren Q.', 3, 'particulier'),
(96, 'F', 'Aiko', 'Davis', 'P.O. Box 972, 7916 Eros Av.', '38383', 'Nancy', '04 26 67 83 28', 'pretium.neque@protonmail.ca', '46c84d2d7a4fc71a06d305e9ea565c1ecad919c8', 1, 'Anderson, Demetrius V.', 4, 'professionnel'),
(97, 'M', 'Yardley', 'Franco', '8123 Mauris. Road', '56367', 'Tournefeuille', '03 53 08 87 25', 'diam@protonmail.org', 'b283b6003adc63d0fcf8b08fff39af75b1f6ff11', 1, 'Decker, Nasim Y.', 5, 'professionnel'),
(98, 'F', 'Mariko', 'Whitfield', '610-6102 Et Rd.', '57862', 'Brive-la-Gaillarde', '05 78 38 16 43', 'enim@protonmail.com', 'f78a8a390667bd7f794c16b314dad768470c3ea0', 0, 'Kirby, Larissa G.', 1, 'particulier'),
(99, 'F', 'Eagan', 'Green', '324-1893 Hendrerit Road', '27842', 'Chartres', '02 98 18 11 53', 'in.faucibus@yahoo.edu', '9191ec0d732d2bb4564b2eba36094b717d92350b', 0, 'Blanchard, Peter Z.', 1, 'professionnel'),
(100, 'F', 'Shannon', 'Workman', 'Ap #331-4493 Nulla. Street', '26562', 'Orvault', '08 44 26 66 72', 'ligula.aenean@yahoo.ca', '7f8d44d77ab7f6e121af5091d5ae28192654e651', 1, 'Brewer, Tashya A.', 4, 'professionnel'),
(101, 'F', 'Abigail', 'Witt', 'Ap #773-1851 Sem Ave', '12618', 'Pessac', '03 68 86 61 52', 'nec.eleifend.non@hotmail.ca', 'a618ff9cac62929606834dd09a23f12cb2d010eb', 1, 'Dixon, Maya H.', 2, 'particulier');

-- --------------------------------------------------------

--
-- Structure de la table `coefficient`
--

CREATE TABLE `coefficient` (
  `nomination` enum('particulier','professionnel') NOT NULL,
  `taux` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `coefficient`
--

INSERT INTO `coefficient` (`nomination`, `taux`) VALUES
('particulier', '19.60'),
('professionnel', '5.50');

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

CREATE TABLE `commandes` (
  `commandeId` int(11) NOT NULL,
  `dateCommande` date NOT NULL,
  `reduction` decimal(15,2) DEFAULT NULL,
  `totalTTC` decimal(15,2) DEFAULT NULL,
  `statut` enum('saisie','annulée','en préparation','expédiée','facturée','soldée') DEFAULT NULL,
  `clientId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `commandes`
--

INSERT INTO `commandes` (`commandeId`, `dateCommande`, `reduction`, `totalTTC`, `statut`, `clientId`) VALUES
(1, '2022-03-11', NULL, NULL, '', 6),
(2, '2021-12-15', NULL, NULL, NULL, 3),
(3, '2021-12-17', NULL, NULL, NULL, 20),
(4, '2022-02-09', NULL, NULL, NULL, 15),
(5, '2022-02-25', NULL, NULL, NULL, 12);

-- --------------------------------------------------------

--
-- Structure de la table `commercial`
--

CREATE TABLE `commercial` (
  `commercialId` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `commercial`
--

INSERT INTO `commercial` (`commercialId`, `nom`, `prenom`) VALUES
(1, 'Sasha', 'Patrick'),
(2, 'Xena', ' Lane'),
(3, 'Elaine', ' Cortez'),
(4, 'Richard', ' Koch'),
(5, 'Kirestin', ' Walters');

-- --------------------------------------------------------

--
-- Structure de la table `factures`
--

CREATE TABLE `factures` (
  `factureId` int(11) NOT NULL,
  `statut` enum('payée','non-payée') DEFAULT NULL,
  `datePaiement` date DEFAULT NULL,
  `commandeId` int(11) NOT NULL,
  `dateCommande` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `factures`
--

INSERT INTO `factures` (`factureId`, `statut`, `datePaiement`, `commandeId`, `dateCommande`) VALUES
(1, '', '2022-03-16', 1, '2022-03-11'),
(2, 'payée', '2022-01-20', 2, '2021-12-15'),
(3, 'payée', '2022-01-12', 3, '2021-12-17'),
(4, 'payée', '2022-03-01', 4, '2022-02-09'),
(5, 'payée', '2022-03-03', 5, '2022-02-25');

-- --------------------------------------------------------

--
-- Structure de la table `fournisseurs`
--

CREATE TABLE `fournisseurs` (
  `fournisseurId` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `fournisseurs`
--

INSERT INTO `fournisseurs` (`fournisseurId`, `nom`) VALUES
(1, 'barraque à plantes'),
(2, 'LVMH'),
(3, 'la Pause calîn♥'),
(4, 'gamme jaune'),
(5, 'sacre Bleu');

-- --------------------------------------------------------

--
-- Structure de la table `lignecommande`
--

CREATE TABLE `lignecommande` (
  `ligneCommandeId` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `prixHT` decimal(15,2) DEFAULT NULL,
  `produitId` int(11) NOT NULL,
  `commandeId` int(11) NOT NULL,
  `dateCommande` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `lignecommande`
--

INSERT INTO `lignecommande` (`ligneCommandeId`, `quantite`, `designation`, `prixHT`, `produitId`, `commandeId`, `dateCommande`) VALUES
(1, NULL, NULL, NULL, 12, 1, '2022-03-11');

-- --------------------------------------------------------

--
-- Structure de la table `livraison`
--

CREATE TABLE `livraison` (
  `livraisonId` int(11) NOT NULL,
  `livraisonDate` date DEFAULT NULL,
  `bonLivraison` varchar(50) DEFAULT NULL,
  `factureId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE `produits` (
  `produitId` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `libelleCourt` varchar(150) DEFAULT NULL,
  `libelleLong` varchar(550) DEFAULT NULL,
  `prixAchat` decimal(15,2) DEFAULT NULL,
  `Photo` varchar(255) DEFAULT NULL,
  `categoriesId` int(11) DEFAULT NULL,
  `categoriesId_1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`produitId`, `nom`, `libelleCourt`, `libelleLong`, `prixAchat`, `Photo`, `categoriesId`, `categoriesId_1`) VALUES
(1, 'dahlias jaunes', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '15.50', 'https://images.pexels.com/photos/2891092/pexels-ph', 6, 2),
(2, 'dahlias bleu', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '11.50', 'https://images.pexels.com/photos/669505/pexels-pho', 6, 1),
(3, 'dahlias blanc', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '17.50', 'https://images.pexels.com/photos/35646/pexels-phot', 6, 2),
(4, 'dahlias rouge', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '16.50', 'https://images.pexels.com/photos/60597/dahlia-red-', 6, 3),
(5, 'rose rouge', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '12.90', 'https://images.pexels.com/photos/7336033/pexels-photo-7336033.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 6, 2),
(6, 'rose blanche', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '13.90', 'https://images.pexels.com/photos/667320/pexels-photo-667320.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 6, 2),
(7, 'Iris', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '8.90', 'https://images.pexels.com/photos/10760330/pexels-photo-10760330.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 6, 3),
(8, 'fleur', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '12.90', 'https://images.pexels.com/photos/670741/pexels-photo-670741.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 6, 3),
(9, 'tournesol', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '10.90', 'https://images.pexels.com/photos/8737346/pexels-photo-8737346.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 6, 1),
(10, 'fleur jaune', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '5.90', 'https://images.pexels.com/photos/9230138/pexels-photo-9230138.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 6, 2),
(11, 'rose rose', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '12.90', 'https://images.pexels.com/photos/4169957/pexels-photo-4169957.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 6, 3),
(12, 'abélias', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '14.90', 'https://images.pexels.com/photos/1030875/pexels-photo-1030875.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 2),
(13, 'Bruyères', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '13.90', 'https://images.pexels.com/photos/3994918/pexels-photo-3994918.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 2),
(14, 'buis', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '10.90', 'https://images.pexels.com/photos/6331041/pexels-photo-6331041.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 1),
(15, 'Cerisier d ornement', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '12.90', 'https://images.pexels.com/photos/5919337/pexels-photo-5919337.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 1),
(16, 'Grenadier', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '15.90', 'https://images.pexels.com/photos/10058566/pexels-photo-10058566.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 1),
(17, 'Hibiscus', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '13.90', 'https://images.pexels.com/photos/4609245/pexels-photo-4609245.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 1),
(18, 'Hortensias', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '16.90', 'https://images.pexels.com/photos/8771016/pexels-photo-8771016.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 1),
(19, 'Houx', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '10.50', 'https://images.pexels.com/photos/10058566/pexels-photo-10058566.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 1),
(20, 'Lilas', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '14.90', 'https://images.pexels.com/photos/4177700/pexels-photo-4177700.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 1),
(21, 'Magnolias', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '12.90', 'https://images.pexels.com/photos/7161186/pexels-photo-7161186.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 1),
(22, 'Rhododendrons', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '12.90', 'https://images.pexels.com/photos/10989296/pexels-photo-10989296.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', 5, 1),
(23, 'Analyse du sol DCM', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '14.50', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/A/n/Analyse-du-sol-DCM-91255-1.jpg', 5, 1),
(24, 'Arrosoir acier galvanisé Rose Tendre ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '19.50', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/A/r/Arrosoir-acier-galvanise-Rose-Tendre-Outillage-copyright-89911-1.jpg', 5, 1),
(25, 'Bâche hivernale noire NETSOL pour un sol propre 1,', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '13.50', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/N/o/Nortene-CELLOPLAST-Bache-hivernale-noire-pour-un-sol-propre-90191-1.jpg', 5, 1),
(26, 'Bandelettes de greffage FLEXIBAND 200 x 6 mm', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '44.50', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/B/a/Bandelettes-de-greffage-FLEXIBAND-200-x-6-mm-copyright-17048-1.jpg', 5, 1),
(27, 'Ceinture porte accessoires Burgon & Ball Bleu euca', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '21.50', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/5/1/51wjfvpsivl._sx425_.jpg', 5, 1),
(28, 'Ceinture porte accessoires pour le jardin Belfast', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '8.50', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/C/e/Ceinture-porte-accessoires-pour-le-jardin-Belfast-99059-1.jpg', 5, 1),
(29, 'Clayette noire pour 3 godets 7 x 7 x 6,4 cm - vend', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '0.50', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/C/l/Clayette-noir-pour-3-godets-7-x-7-x6_4-cm-vendu-par-5-98862-1.jpg', 5, 1),
(30, 'Clayette noire pour 4 godets 7 x 7 x 6,4 cm - vend', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '0.58', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/C/l/Clayette-noir-pour-4-godets-7-x-7-x6_4-cm-vendu-par-5-98864-1.jpg', 5, 1),
(31, 'Cloche à salade transparente PVC Ø 33 cm - vendu p', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '6.17', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/N/o/Nortene-CELLOPLAST-Cloche-a-salade-transparente-PVC-Lot-de3-90146-1.jpg', 5, 1),
(32, 'Clou polypropylène pour paillages et manchons de d', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '0.35', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/C/l/Clou-polypropylene-pour-paillages-et-manchons-de-dissuasion-91077-1.jpg', 5, 1),
(33, 'Dalle carrée de paillage naturel biodégradable Cha', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '3.50', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/820x/9df78eab33525d08d6e5fb8d27136e95/N/o/Nortene-CELLOPLAST-Paillage-naturel-en-chanvre-0_7-et-lin500g-90077-1.jpg', 5, 1),
(34, 'Robot tondeuse connecté MCCULLOCH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '677.18', 'https://media.adeo.com/marketplace/LMFR/82039885/b901e12a-de0e-42db-9693-d954a40bb2db.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1),
(35, 'Tondeuse autoportée éjection arrière MTD LT 92 EXT', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '864.50', 'https://media.adeo.com/marketplace/82376411/d6874d5a-720e-4c6a-83bb-88ff48d71f02.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1),
(36, 'Tondeuse thermique STERWINS, 140cm³', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '349.00', 'https://media.adeo.com/marketplace/LMFR/81967948/9f94f566-7ca4-49f8-9137-e5b9b33027da.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1),
(37, 'Débroussailleuse sur batterie RYOBI One+ ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '189.00', 'https://media.adeo.com/marketplace/74045615/d21cca0a-97b0-407a-adfd-f104e3b8e8cc.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1),
(38, 'Débroussailleuse à roues à essence MTD Wst5522 ', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '360.50', 'https://media.adeo.com/marketplace/19514992/05d66c6b-8415-411d-aa76-125420e9b348.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1),
(39, 'Débroussailleuse à essence STIHL', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '468.50', 'https://media.adeo.com/marketplace/63115976/04b45e54-d5b1-430e-ade8-42d9b7722360.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1),
(40, 'Taille-haie électrique Hte01.3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '28.90', 'https://media.adeo.com/marketplace/LMFR/19392814/c325e55d-4b35-4d55-a359-90ff163d21ee.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1),
(41, 'Taille-haie électrique électrique BOSCH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '65.50', 'https://media.adeo.com/marketplace/LMFR/89144955/8e8cc662-3f43-4aca-afae-248677cffe49.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1),
(42, 'Taille-haie sur perche électrique STERWINS', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '94.90', 'https://media.adeo.com/marketplace/19300274/ab14b800-c6dd-4ea6-8b6e-2cf05551bb31.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1),
(43, 'Bineuse électrique EINHELL', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '156.42', 'https://media.adeo.com/marketplace/LMFR/82804863/fd6cab2d-aa85-4659-bc7c-677edaf22359.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1),
(44, 'Motobineuse à essence Marche avant et arrière', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque scelerisque arcu quis vestibulum vulputate', 'Ut vel nunc eget odio pellentesque viverra a eget eros. Praesent vitae eros at felis dapibus semper. Nullam quis risus porttitor risus fermentum dignissim eu sit amet enim. Nam sed iaculis odio. Proin risus turpis, pellentesque a tellus vitae, mollis tempor libero. Nulla id nibh at enim gravida pharetra in a felis. Sed a faucibus felis. Aliquam erat volutpat. Maecenas tincidunt nisl at interdum mattis.', '449.00', 'https://media.adeo.com/marketplace/82945979/1591bee7-1cb8-4d96-91ea-4216aef25003.jpeg?width=750&height=750&format=jpg&quality=80&fit=bounds', 5, 1);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `produits_fournisseur`
-- (Voir ci-dessous la vue réelle)
--
CREATE TABLE `produits_fournisseur` (
`produitId` int(11)
,`nomProduit` varchar(50)
,`libelleCourt` varchar(150)
,`libelleLong` varchar(550)
,`prixAchat` decimal(15,2)
,`Photo` varchar(255)
,`categoriesId` int(11)
,`categoriesId_1` int(11)
,`fournisseurId` int(11)
,`nomFournisseur` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure de la table `stocker`
--

CREATE TABLE `stocker` (
  `produitId` int(11) NOT NULL,
  `fournisseurId` int(11) NOT NULL,
  `quantite` int(11) DEFAULT 2
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `stocker`
--

INSERT INTO `stocker` (`produitId`, `fournisseurId`, `quantite`) VALUES
(1, 1, 5),
(2, 1, 10),
(3, 1, 34),
(4, 4, 12),
(5, 4, 2),
(6, 1, 24),
(7, 1, 23),
(8, 4, 32),
(9, 1, 22),
(10, 4, 42),
(11, 5, 26),
(12, 2, 53),
(13, 3, 22),
(14, 3, 62),
(15, 3, 62),
(16, 3, 51),
(17, 2, 2),
(18, 2, 57),
(19, 3, 45),
(20, 3, 35),
(21, 4, 13),
(22, 4, 25),
(23, 2, 57),
(24, 3, 34),
(25, 1, 12),
(26, 2, 28),
(27, 3, 75),
(28, 4, 32),
(29, 4, 28),
(30, 2, 42),
(31, 3, 87),
(32, 1, 45),
(33, 1, 58),
(34, 3, 38),
(35, 3, 46),
(36, 1, 38),
(37, 4, 38),
(38, 4, 27),
(39, 2, 57),
(40, 3, 38),
(41, 1, 64),
(42, 1, 35),
(43, 1, 69),
(44, 4, 46);

-- --------------------------------------------------------

--
-- Structure de la vue `produits_fournisseur`
--
DROP TABLE IF EXISTS `produits_fournisseur`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `produits_fournisseur`  AS SELECT `produits`.`produitId` AS `produitId`, `produits`.`nom` AS `nomProduit`, `produits`.`libelleCourt` AS `libelleCourt`, `produits`.`libelleLong` AS `libelleLong`, `produits`.`prixAchat` AS `prixAchat`, `produits`.`Photo` AS `Photo`, `produits`.`categoriesId` AS `categoriesId`, `produits`.`categoriesId_1` AS `categoriesId_1`, `fournisseurs`.`fournisseurId` AS `fournisseurId`, `fournisseurs`.`nom` AS `nomFournisseur` FROM ((`produits` join `fournisseurs`) join `stocker`) WHERE `produits`.`produitId` = `stocker`.`produitId` AND `fournisseurs`.`fournisseurId` = `stocker`.`fournisseurId` ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `archive`
--
ALTER TABLE `archive`
  ADD PRIMARY KEY (`clientId`,`commandeId`,`dateCommande`),
  ADD KEY `commandeId` (`commandeId`,`dateCommande`);

--
-- Index pour la table `avoir`
--
ALTER TABLE `avoir`
  ADD PRIMARY KEY (`produitId`,`categoriesId`),
  ADD KEY `categoriesId` (`categoriesId`);

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categoriesId`),
  ADD KEY `categoriesId_1` (`categoriesId_1`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`clientId`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `pwd` (`pwd`),
  ADD KEY `commercialId` (`commercialId`),
  ADD KEY `nomination` (`nomination`);

--
-- Index pour la table `coefficient`
--
ALTER TABLE `coefficient`
  ADD PRIMARY KEY (`nomination`);

--
-- Index pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD PRIMARY KEY (`commandeId`,`dateCommande`),
  ADD KEY `clientId` (`clientId`);

--
-- Index pour la table `commercial`
--
ALTER TABLE `commercial`
  ADD PRIMARY KEY (`commercialId`);

--
-- Index pour la table `factures`
--
ALTER TABLE `factures`
  ADD PRIMARY KEY (`factureId`),
  ADD UNIQUE KEY `commandeId` (`commandeId`,`dateCommande`);

--
-- Index pour la table `fournisseurs`
--
ALTER TABLE `fournisseurs`
  ADD PRIMARY KEY (`fournisseurId`);

--
-- Index pour la table `lignecommande`
--
ALTER TABLE `lignecommande`
  ADD PRIMARY KEY (`ligneCommandeId`),
  ADD KEY `produitId` (`produitId`),
  ADD KEY `commandeId` (`commandeId`,`dateCommande`);

--
-- Index pour la table `livraison`
--
ALTER TABLE `livraison`
  ADD PRIMARY KEY (`livraisonId`),
  ADD KEY `factureId` (`factureId`);

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`produitId`),
  ADD KEY `categories_ibfk_2` (`categoriesId`),
  ADD KEY `categories_ibfk_3` (`categoriesId_1`);

--
-- Index pour la table `stocker`
--
ALTER TABLE `stocker`
  ADD PRIMARY KEY (`produitId`,`fournisseurId`),
  ADD KEY `fournisseurId` (`fournisseurId`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `archive`
--
ALTER TABLE `archive`
  ADD CONSTRAINT `archive_ibfk_1` FOREIGN KEY (`clientId`) REFERENCES `client` (`clientId`),
  ADD CONSTRAINT `archive_ibfk_2` FOREIGN KEY (`commandeId`,`dateCommande`) REFERENCES `commandes` (`commandeId`, `dateCommande`);

--
-- Contraintes pour la table `avoir`
--
ALTER TABLE `avoir`
  ADD CONSTRAINT `avoir_ibfk_1` FOREIGN KEY (`produitId`) REFERENCES `produits` (`produitId`),
  ADD CONSTRAINT `avoir_ibfk_2` FOREIGN KEY (`categoriesId`) REFERENCES `categories` (`categoriesId`);

--
-- Contraintes pour la table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`categoriesId_1`) REFERENCES `categories` (`categoriesId`);

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `client_ibfk_1` FOREIGN KEY (`commercialId`) REFERENCES `commercial` (`commercialId`),
  ADD CONSTRAINT `client_ibfk_2` FOREIGN KEY (`nomination`) REFERENCES `coefficient` (`nomination`);

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_ibfk_1` FOREIGN KEY (`clientId`) REFERENCES `client` (`clientId`);

--
-- Contraintes pour la table `factures`
--
ALTER TABLE `factures`
  ADD CONSTRAINT `factures_ibfk_1` FOREIGN KEY (`commandeId`,`dateCommande`) REFERENCES `commandes` (`commandeId`, `dateCommande`);

--
-- Contraintes pour la table `lignecommande`
--
ALTER TABLE `lignecommande`
  ADD CONSTRAINT `lignecommande_ibfk_1` FOREIGN KEY (`produitId`) REFERENCES `produits` (`produitId`),
  ADD CONSTRAINT `lignecommande_ibfk_2` FOREIGN KEY (`commandeId`,`dateCommande`) REFERENCES `commandes` (`commandeId`, `dateCommande`);

--
-- Contraintes pour la table `livraison`
--
ALTER TABLE `livraison`
  ADD CONSTRAINT `livraison_ibfk_1` FOREIGN KEY (`factureId`) REFERENCES `factures` (`factureId`);

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`categoriesId`) REFERENCES `categories` (`categoriesId`),
  ADD CONSTRAINT `categories_ibfk_3` FOREIGN KEY (`categoriesId_1`) REFERENCES `categories` (`categoriesId_1`);

--
-- Contraintes pour la table `stocker`
--
ALTER TABLE `stocker`
  ADD CONSTRAINT `stocker_ibfk_1` FOREIGN KEY (`produitId`) REFERENCES `produits` (`produitId`),
  ADD CONSTRAINT `stocker_ibfk_2` FOREIGN KEY (`fournisseurId`) REFERENCES `fournisseurs` (`fournisseurId`);




UPDATE client
SET pwd = SHA1(pwd);

pour sauvegarder:
mysqldump -u root green_garden > saveGreenGarden.sql

Pour restaurer :
mysqldump -u root green_garden < saveGreenGarden.sql


DELIMITER |
CREATE PROCEDURE moyenneDelai ()
SELECT commandeId, AVG(datediff(datePaiement, dateCommande))
FROM `factures`;
END |
DELIMITER;

SELECT fournisseurs.nom, SUM(lignecommande.prixHT) 
from fournisseurs, stocker, lignecommande 
WHERE fournisseurs.fournisseurId = stocker.fournisseurId 
AND stocker.produitId = lignecommande.produitId 
GROUP BY fournisseurs.nom;













COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
